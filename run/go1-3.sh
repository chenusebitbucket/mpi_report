#!/bin/sh
#PJM -N "S1-3"
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=00:15:00
#PJM -g gt37
#PJM -j
#PJM -e error.err
#PJM -o S1-3.output

mpiexec.hydra -n ${PJM_MPI_PROC} ./S1-3
