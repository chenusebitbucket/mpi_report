#!/bin/sh
#PJM -N "S2"
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=00:15:00
#PJM -g gt37
#PJM -j
#PJM -e error.err
#PJM -o S2.output

./S2_np
mpiexec.hydra -n ${PJM_MPI_PROC} ./S2_p
