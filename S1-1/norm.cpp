#include <mpi.h>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

int main(int argc, char const *argv[])
{
    int num_processors, processor_id;
    std::string temp;

    std::vector<double> v1(8), v2;
    std::string a1_prefix = "a1.";
    std::string a2_prefix = "a2.";
    double total_sum1, total_sum2;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &num_processors);
    MPI_Comm_rank(MPI_COMM_WORLD, &processor_id);

    std::ifstream a1(a1_prefix + std::to_string(processor_id));
    std::ifstream a2(a2_prefix + std::to_string(processor_id));
    for (int i = 0; i < 8; ++i) {
        getline(a1, temp);
        v1[i] = std::stod(temp);
    }
    getline(a2, temp);
    int num_elements = std::stoi(temp);
    v2.resize(num_elements);
    for (int i = 0; i < num_elements; ++i) {
        getline(a2, temp);
        v2[i] = std::stod(temp);
    }
    double sum1 = std::inner_product(v1.begin(), v1.end(), v1.begin(), 0.0);
    double sum2 = std::inner_product(v2.begin(), v2.end(), v2.begin(), 0.0);

    MPI_Allreduce(&sum1, &total_sum1, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&sum2, &total_sum2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    total_sum1 = sqrt(total_sum1);
    total_sum2 = sqrt(total_sum2);

    if (processor_id == 0) {
        std::cout << "Norm of a1 = " << std::scientific << std::setprecision(16)
                  << total_sum1 << std::endl;
        std::cout << "Norm of a2 = " << total_sum2 << std::endl;
    }

    MPI_Finalize();
    return 0;
}
