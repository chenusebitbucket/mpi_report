#include <mpi.h>
#include <sys/time.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

void Type_a(int num_segments, int num_processors, int processor_id,
            double area);
void Type_b(int num_segments, int num_processors, int processor_id,
            double area);

int main(int argc, char const *argv[])
{
    int num_processors, processor_id;
    std::string temp;

    int num_segments;
    double area1, area2;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &num_processors);
    MPI_Comm_rank(MPI_COMM_WORLD, &processor_id);

    std::ifstream file("number.dat");
    getline(file, temp);
    num_segments = std::stoi(temp);

    Type_a(num_segments, num_processors, processor_id, area1);
    Type_b(num_segments, num_processors, processor_id, area2);

    MPI_Finalize();
    return 0;
}

void Type_a(int num_segments, int num_processors, int processor_id, double area)
{
    double start_time, end_time;
    double area_local = 0.0;

    int start_index = processor_id * num_segments / num_processors;
    int end_index = (processor_id + 1) * num_segments / num_processors;
    double dx = 1.0 / num_segments;

    MPI_Barrier(MPI_COMM_WORLD);
    start_time = MPI_Wtime();
    double x0, x1, f0, f1;
    for (int i = start_index; i < end_index; ++i) {
        x0 = static_cast<double>(i) * dx;
        x1 = static_cast<double>(i + 1) * dx;
        f0 = 4.0 / (1.0 + x0 * x0);
        f1 = 4.0 / (1.0 + x1 * x1);
        area_local += 0.5 * (f0 + f1) * dx;
    }
    MPI_Reduce(&area_local, &area, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    end_time = MPI_Wtime();

    if (processor_id == 0) {
        std::cout << "Type a: Integration result = " << std::setprecision(16)
                  << area << ", time = " << end_time - start_time << std::endl;
    }
}

void Type_b(int num_segments, int num_processors, int processor_id, double area)
{
    double start_time, end_time;
    double area_local = 0.0;

    double dx = 1.0 / num_segments;

    MPI_Barrier(MPI_COMM_WORLD);
    start_time = MPI_Wtime();
    double x0, x1, f0, f1;
    for (int i = processor_id; i < num_segments; i += num_processors) {
        x0 = static_cast<double>(i) * dx;
        x1 = static_cast<double>(i + 1) * dx;
        f0 = 4.0 / (1.0 + x0 * x0);
        f1 = 4.0 / (1.0 + x1 * x1);
        area_local += 0.5 * (f0 + f1) * dx;
    }
    MPI_Reduce(&area_local, &area, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    end_time = MPI_Wtime();

    if (processor_id == 0) {
        std::cout << "Type b: Integration result = " << std::setprecision(16)
                  << area << ", time = " << end_time - start_time << std::endl;
    }
}
