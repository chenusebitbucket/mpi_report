# MPI Report
This is a code repository for the report for Technical & Scientific Computing II (2019) at the University of Tokyo.

## Author
- Yen-Chen Chen (Department of Mathematical Informatics, the University of Tokyo)

## Course Instructor
- Kengo Nakajima
