#include <mpi.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int main(int argc, char const *argv[])
{
    int num_processors, processor_id;
    std::string temp;

    std::vector<double> v2, v_gather;
    std::string a2_prefix = "a2.";
    std::vector<int> recieve_counts, displacements;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &num_processors);
    MPI_Comm_rank(MPI_COMM_WORLD, &processor_id);

    recieve_counts.reserve(num_processors);
    displacements.reserve(num_processors + 1);

    std::ifstream a2(a2_prefix + std::to_string(processor_id));
    getline(a2, temp);
    int num_elements = std::stoi(temp);
    v2.reserve(num_elements);
    for (int i = 0; i < num_elements; ++i) {
        getline(a2, temp);
        v2[i] = std::stod(temp);
    }

    MPI_Allgather(&num_elements, 1, MPI_INT, &recieve_counts[0], 1, MPI_INT,
                  MPI_COMM_WORLD);

    displacements[0] = 0.0;
    for (int i = 0; i < num_processors; ++i) {
        displacements[i + 1] = displacements[i] + recieve_counts[i];
    }

    v_gather.reserve(displacements[num_processors]);
    MPI_Allgatherv(&v2[0], num_elements, MPI_DOUBLE, &v_gather[0],
                   &recieve_counts[0], &displacements[0], MPI_DOUBLE,
                   MPI_COMM_WORLD);
    if (processor_id == 0) {
        std::cout << "Gathered vector = " << std::endl;
        for (int i = 0; i < displacements[num_processors]; ++i) {
            std::cout << v_gather[i] << std::endl;
        }
    }

    MPI_Finalize();
    return 0;
}
