#include <sys/time.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include <sparse.hpp>


void CG(Sparse A, std::vector<double> &x, std::vector<double> b,
        int max_iteration, double tolerance);


int main(int argc, char const *argv[])
{
    struct timeval start_time, end_time;

    std::string input("input.dat");
    std::string line;

    std::ifstream file(input);

    getline(file, line);
    int num_elements = std::stoi(line);
    num_elements++;

    getline(file, line);
    double dx, heat, area,
        lambda;  // lambda represents the thermal conductivity
    sscanf(line.c_str(), "%lf %lf %lf %lf", &dx, &heat, &area, &lambda);

    getline(file, line);
    int max_iteration = std::stoi(line);

    getline(file, line);
    double tolerance = std::stod(line);

    // Setup linear system Ax=b
    Sparse A(num_elements);
    A.SetValues(dx, area, lambda);
    double b_value = 0.5 * heat * area * dx;
    std::vector<double> x(num_elements, 0.0), b(num_elements, b_value);

    std::cout << "Nonparallel Version" << std::endl;

    gettimeofday(&start_time, NULL);

    CG(A, x, b, max_iteration, tolerance);

    gettimeofday(&end_time, NULL);

    std::cout << "Time = " << std::scientific
              << ((end_time.tv_sec - start_time.tv_sec) * 1000000u +
                  end_time.tv_usec - start_time.tv_usec) /
                     1.e6
              << std::endl;

    return 0;
}

void CG(Sparse A, std::vector<double> &x, std::vector<double> b,
        int max_iteration, double tolerance)
{
    int N = A.size();
    std::vector<double> z(N);
    std::vector<double> p(N);
    std::vector<double> q(N);
    double rho, rho_last, alpha, beta;
    int iter;

    // Preconditioner M = diag(A)^-1
    std::vector<double> M(N);
    for (int i = 0; i < N; ++i) {
        M[i] = 1.0 / A.at(i, i);
    }

    // R0 = b - A * x0
    std::vector<double> r(N, b[0]);
    std::vector<double> Ax = A * x;
    for (int i = 0; i < N; ++i) {
        r[i] = r[i] - Ax[i];
    }
    double B_norm_square =
        std::inner_product(r.begin(), r.end(), r.begin(), 0.0);
    double D_norm_square, residual;

    for (iter = 0; iter < max_iteration; ++iter) {
        // z = M * r
        for (int i = 0; i < N; ++i) {
            z[i] = M[i] * r[i];
        }

        // rho = dot(r, z)
        rho = std::inner_product(r.begin(), r.end(), z.begin(), 0.0);

        // beta = rho / rho_last
        // p = z + beta * p
        if (iter == 0) {
            p = z;
        } else {
            beta = rho / rho_last;
            for (int i = 0; i < N; ++i) {
                p[i] = z[i] + beta * p[i];
            }
        }

        // q = A * p
        q = A * p;

        // alpha = rho / dot(p, q)
        alpha = rho / std::inner_product(p.begin(), p.end(), q.begin(), 0.0);

        // x = x + alpha * p
        // r = r - alpha * q
        for (int i = 0; i < N; ++i) {
            x[i] += alpha * p[i];
            r[i] -= alpha * q[i];
        }

        D_norm_square = std::inner_product(r.begin(), r.end(), r.begin(), 0.0);

        residual = std::sqrt(D_norm_square / B_norm_square);

        if (residual <= tolerance) break;

        rho_last = rho;
    }

    std::cout << "iter = " << iter << ", residual = " << residual << std::endl;
}
