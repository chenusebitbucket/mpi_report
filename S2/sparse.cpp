#include <iostream>

#include <sparse.hpp>

void Sparse::SetValues(double dx, double area, double lambda)
{
    double temp = area * lambda / dx;
    for (int i = 0; i < this->num_elements_; ++i) {
        this->diagonal_[i] = 2 * temp;
    }
    if (this->processor_id_ == 0) {
        this->diagonal_[0] = temp;
    }
    if (this->processor_id_ == this->num_processors_ - 1) {
        this->diagonal_[num_elements_ - 1] = temp;
    }
    for (int i = 0; i < this->upper_skew_.size(); ++i) {
        this->upper_skew_[i] = -temp;
    }
    for (int i = 0; i < this->lower_skew_.size(); ++i) {
        this->lower_skew_[i] = -temp;
    }
    if (this->processor_id_ == 0) {
        this->upper_skew_[0] = 0;
        this->lower_skew_[0] = 0;
    }
}

std::vector<double> Sparse::operator*(std::vector<double> x)
{
    int N = this->num_elements_;
    std::vector<double> b(N, 0.0);

    b[0] = this->at(0, 0) * x[0] + this->at(0, 1) * x[1];
    for (int i = 1; i < N - 1; ++i) {
        for (int j = i - 1; j <= i + 1; ++j) {
            b[i] += this->at(i, j) * x[j];
        }
    }
    b[N - 1] =
        this->at(N - 2, N - 1) * x[N - 2] + this->at(N - 1, N - 1) * x[N - 1];

    return b;
}
