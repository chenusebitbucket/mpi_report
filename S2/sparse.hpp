#ifndef SPARSE_HPP_
#define SPARSE_HPP_

#include <iostream>
#include <stdexcept>
#include <vector>

class Sparse {
public:
    Sparse(int N, int num_processors, int processor_id)
    {
        this->num_processors_ = num_processors;
        this->processor_id_ = processor_id;
        this->num_elements_ = N;
        this->diagonal_.resize(N);
        if (num_processors == 1) {
            this->type_ = 0;
        } else {
            if (processor_id == 0) {
                this->type_ = 1;
            } else if (processor_id == num_processors - 1) {
                this->type_ = 3;
            } else {
                this->type_ = 2;
            }
        }

        if (this->type_ == 0 || this->type_ == 3) {
            this->upper_skew_.resize(N - 1);
        } else {
            this->upper_skew_.resize(N);
        }
        if (this->type_ == 0 || this->type_ == 1) {
            this->lower_skew_.resize(N - 1);
        } else {
            this->lower_skew_.resize(N);
        }
    };

    Sparse(int N) : Sparse(N, 1, 0){};

    int size() { return this->num_elements_; };

    double at(int row, int column)
    {
        if (row == column) {
            return this->diagonal_[row];
        } else if (row == column - 1) {
            return this->upper_skew_[row];
        } else if (row == column + 1) {
            return this->lower_skew_[column];
        } else {
            return 0.0;
        }
    };

    Sparse(){};

    void PrintValues()
    {
        std::cout << "A = " << std::endl;
        for (int i = 0; i < this->num_elements_; ++i) {
            for (int j = 0; j < this->num_elements_; ++j) {
                std::cout << this->at(i, j) << " ";
            }
            std::cout << std::endl;
        }
    }

    void SetValues(double dx, double area, double lambda);

    std::vector<double> operator*(std::vector<double> x);

private:
    int num_processors_;
    int processor_id_;
    int type_;  // type 0: full matrix,
                // type 1: first processor,
                // type 2: middle processors,
                // type 3: last processor
    int num_elements_;

    std::vector<double> diagonal_;
    std::vector<double> upper_skew_;
    std::vector<double> lower_skew_;
};

#endif  // SPARSE_HPP_
