#include <mpi.h>
#include <cmath>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>

#include <sparse.hpp>

void CG(Sparse A, std::vector<double> &x, std::vector<double> b,
        int max_iteration, double tolerance, int N, int num_processors,
        int processor_id);

void parallel_multiple(std::vector<double> &Ax, Sparse A, std::vector<double> x,
                       int num_elements, int num_processors, int processor_id);


int main(int argc, char const *argv[])
{
    int num_processors, processor_id;
    double start_time, end_time;

    std::string input("input.dat");
    std::string line;

    MPI_Init(NULL, NULL);
    MPI_Comm_size(MPI_COMM_WORLD, &num_processors);
    MPI_Comm_rank(MPI_COMM_WORLD, &processor_id);

    int N;
    double dx, heat, area,
        lambda;  // lambda represents the thermal conductivity
    int max_iteration;
    double tolerance;

    if (processor_id == 0) {
        std::ifstream file(input);

        getline(file, line);
        N = std::stoi(line);
        N++;

        getline(file, line);

        sscanf(line.c_str(), "%lf %lf %lf %lf", &dx, &heat, &area, &lambda);

        getline(file, line);
        max_iteration = std::stoi(line);

        getline(file, line);
        tolerance = std::stod(line);
    }

    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&dx, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&heat, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&area, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&lambda, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&max_iteration, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&tolerance, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    int num_elements = N / num_processors;
    if (processor_id < N - num_elements * num_processors) num_elements++;

    Sparse A(num_elements, num_processors, processor_id);
    A.SetValues(dx, area, lambda);
    double b_value = 0.5 * heat * area * dx;
    std::vector<double> x(num_elements, 0.0), b(num_elements, b_value);

    if (processor_id == 0) std::cout << "Parallel Version!" << std::endl;

    MPI_Barrier(MPI_COMM_WORLD);
    start_time = MPI_Wtime();

    CG(A, x, b, max_iteration, tolerance, N, num_processors, processor_id);

    MPI_Barrier(MPI_COMM_WORLD);
    end_time = MPI_Wtime();

    if (processor_id == 0)
        std::cout << "Time = " << end_time - start_time << std::endl;

    MPI_Finalize();
    return 0;
}

void CG(Sparse A, std::vector<double> &x, std::vector<double> b,
        int max_iteration, double tolerance, int N, int num_processors,
        int processor_id)
{
    int num_elements = N / num_processors;
    if (processor_id < N - num_elements * num_processors) num_elements++;
    std::vector<double> z(num_elements);
    std::vector<double> p(num_elements);
    std::vector<double> q(num_elements);
    double rho, rho_last, alpha, beta;
    int iter;

    // Preconditioner M = diag(A)^-1
    std::vector<double> M(num_elements);
    for (int i = 0; i < num_elements; ++i) {
        M[i] = 1.0 / A.at(i, i);
    }

    // R0 = b - A * x0
    std::vector<double> r(num_elements, b[0]);
    std::vector<double> Ax(num_elements);
    parallel_multiple(Ax, A, x, num_elements, num_processors, processor_id);
    for (int i = 0; i < num_elements; ++i) {
        r[i] = r[i] - Ax[i];
    }

    double B_norm_square_local =
        std::inner_product(r.begin(), r.end(), r.begin(), 0.0);
    double B_norm_square;
    MPI_Allreduce(&B_norm_square_local, &B_norm_square, 1, MPI_DOUBLE, MPI_SUM,
                  MPI_COMM_WORLD);
    double D_norm_square, residual;

    for (iter = 0; iter < max_iteration; ++iter) {
        // z = M * r
        for (int i = 0; i < num_elements; ++i) {
            z[i] = M[i] * r[i];
        }

        // rho = dot(r, z)
        double rho_local =
            std::inner_product(r.begin(), r.end(), z.begin(), 0.0);
        MPI_Allreduce(&rho_local, &rho, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        // beta = rho / rho_last
        // p = z + beta * p
        if (iter == 0) {
            p = z;
        } else {
            beta = rho / rho_last;
            for (int i = 0; i < num_elements; ++i) {
                p[i] = z[i] + beta * p[i];
            }
        }

        // q = A * p
        parallel_multiple(q, A, p, num_elements, num_processors, processor_id);

        // alpha = rho / dot(p, q)
        double pq;
        double pq_local =
            std::inner_product(p.begin(), p.end(), q.begin(), 0.0);
        MPI_Allreduce(&pq_local, &pq, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        alpha = rho / pq;

        // x = x + alpha * p
        // r = r - alpha * q
        for (int i = 0; i < num_elements; ++i) {
            x[i] += alpha * p[i];
            r[i] -= alpha * q[i];
        }

        double D_norm_square_local =
            std::inner_product(r.begin(), r.end(), r.begin(), 0.0);
        MPI_Allreduce(&D_norm_square_local, &D_norm_square, 1, MPI_DOUBLE,
                      MPI_SUM, MPI_COMM_WORLD);

        residual = std::sqrt(D_norm_square / B_norm_square);

        if (residual <= tolerance) break;

        rho_last = rho;
    }

    if (processor_id == 0) {
        std::cout << "iter = " << iter << ", residual = " << residual
                  << std::endl;
    }
}

void parallel_multiple(std::vector<double> &Ax, Sparse A, std::vector<double> x,
                       int num_elements, int num_processors, int processor_id)
{
    MPI_Request *send_request;
    MPI_Request *recv_request;
    MPI_Status *send_status;
    MPI_Status *recv_status;

    if (processor_id == 0) {
        send_request = new MPI_Request[1];
        recv_request = new MPI_Request[1];
        send_status = new MPI_Status[1];
        recv_status = new MPI_Status[1];
        MPI_Isend(&x[num_elements - 1], 1, MPI_DOUBLE, 1, 1, MPI_COMM_WORLD,
                  &send_request[0]);
        double x_end;
        MPI_Irecv(&x_end, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD,
                  &recv_request[0]);
        MPI_Waitall(1, send_request, send_status);
        MPI_Waitall(1, recv_request, recv_status);
        if (num_elements == 1) {
            Ax[0] = A.at(0, 0) * x[0] + A.at(0, 1) * x_end;
        } else {
            Ax[0] = A.at(0, 0) * x[0] + A.at(0, 1) * x[1];
        }
        for (int i = 1; i < num_elements - 1; ++i) {
            Ax[i] = A.at(i, i - 1) * x[i - 1] + A.at(i, i) * x[i] +
                    A.at(i, i + 1) * x[i + 1];
        }
        if (num_elements > 1) {
            Ax[num_elements - 1] =
                A.at(num_elements - 1, num_elements - 2) * x[num_elements - 2] +
                A.at(num_elements - 1, num_elements - 1) * x[num_elements - 1] +
                A.at(num_elements - 1, num_elements) * x_end;
        }
    } else if (processor_id == num_processors - 1) {
        send_request = new MPI_Request[1];
        recv_request = new MPI_Request[1];
        send_status = new MPI_Status[1];
        recv_status = new MPI_Status[1];
        MPI_Isend(&x[0], 1, MPI_DOUBLE, num_processors - 2,
                  2 * num_processors - 4, MPI_COMM_WORLD, &send_request[0]);
        double x_start;
        MPI_Irecv(&x_start, 1, MPI_DOUBLE, num_processors - 2,
                  2 * num_processors - 3, MPI_COMM_WORLD, &recv_request[0]);
        MPI_Waitall(1, send_request, send_status);
        MPI_Waitall(1, recv_request, recv_status);
        if (num_elements > 1) {
            Ax[0] =
                A.at(0, 1) * x_start + A.at(0, 0) * x[0] + A.at(0, 1) * x[1];
        }
        for (int i = 1; i < num_elements - 1; ++i) {
            Ax[i] = A.at(i, i + 1) * x[i - 1] + A.at(i, i) * x[i] +
                    A.at(i + 1, i) * x[i + 1];
        }
        if (num_elements == 1) {
            Ax[0] = A.at(1, 0) * x_start + A.at(0, 0) * x[1 - 1];
        } else {
            Ax[num_elements - 1] =
                A.at(num_elements, num_elements - 1) * x[num_elements - 2] +
                A.at(num_elements - 1, num_elements - 1) * x[num_elements - 1];
        }
    } else {
        send_request = new MPI_Request[2];
        recv_request = new MPI_Request[2];
        send_status = new MPI_Status[2];
        recv_status = new MPI_Status[2];
        MPI_Isend(&x[0], 1, MPI_DOUBLE, processor_id - 1,
                  2 * (processor_id - 1), MPI_COMM_WORLD, &send_request[0]);
        MPI_Isend(&x[num_elements - 1], 1, MPI_DOUBLE, processor_id + 1,
                  2 * processor_id + 1, MPI_COMM_WORLD, &send_request[1]);
        double x_start, x_end;
        MPI_Irecv(&x_start, 1, MPI_DOUBLE, processor_id - 1,
                  2 * processor_id - 1, MPI_COMM_WORLD, &recv_request[0]);
        MPI_Irecv(&x_end, 1, MPI_DOUBLE, processor_id + 1, 2 * processor_id,
                  MPI_COMM_WORLD, &recv_request[1]);
        MPI_Waitall(2, send_request, send_status);
        MPI_Waitall(2, recv_request, recv_status);
        if (num_elements == 1) {
            Ax[0] =
                A.at(1, 0) * x_start + A.at(0, 0) * x[0] + A.at(0, 1) * x_end;
        }
        if (num_elements > 1) {
            Ax[0] =
                A.at(1, 0) * x_start + A.at(0, 0) * x[0] + A.at(0, 1) * x[1];
        }
        for (int i = 1; i < num_elements - 1; ++i) {
            Ax[i] = A.at(i, i + 1) * x[i - 1] + A.at(i, i) * x[i] +
                    A.at(i + 1, i) * x[i + 1];
        }
        if (num_elements > 1) {
            Ax[num_elements - 1] =
                A.at(num_elements, num_elements - 1) * x[num_elements - 2] +
                A.at(num_elements - 1, num_elements - 1) * x[num_elements - 1] +
                A.at(num_elements - 1, num_elements) * x_end;
        }
    }
}
